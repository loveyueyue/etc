package com.yueyue.etc.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * 时间工具类
 *
 * @author pengjian
 * @date 2021-08-14 00:07
 */
public class DateUtils {
    /**
     * yy-MM-dd
     */
    public static final String FORMAT_YY_MM_DD = "yyyy-MM-dd";

    public static final String FORMAT_YY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";


    /**
     * 获得当前时间
     *
     * @return 当前时间
     */
    public static String getNow() {
        LocalDate today = LocalDate.now();
        return today.toString();
    }

    /**
     * 获取今天开始时间
     *
     * @return 开始时间
     */
    public static String getTodayStart() {
        LocalDateTime today = LocalDateTime.of(LocalDate.now(), LocalTime.MIN);
        return today.format(DateTimeFormatter.ofPattern(FORMAT_YY_MM_DD_HH_MM_SS));
    }

    /**
     * 获取今天结束时间
     *
     * @return 结束时间
     */
    public static String getTodayEnd() {
        LocalDateTime today = LocalDateTime.of(LocalDate.now(), LocalTime.MAX);
        return today.format(DateTimeFormatter.ofPattern(FORMAT_YY_MM_DD_HH_MM_SS));
    }

    /**
     * LocalDate 转 Date
     *
     * @param localDate localDate
     * @return Date
     */
    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * LocalDateTime 转 Date
     *
     * @param localDateTime localDateTime
     * @return Date
     */
    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Date 转 LocalDate
     *
     * @param date date
     * @return LocalDate
     */
    public static LocalDate asLocalDate(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    /**
     * Date 转 LocalDateTime
     *
     * @param date date
     * @return LocalDateTime
     */
    public static LocalDateTime asLocalDateTime(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
