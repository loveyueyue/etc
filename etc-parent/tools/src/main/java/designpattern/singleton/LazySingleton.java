package designpattern.singleton;

/**
 * 懒汉模式
 *
 * @author pengjian
 * @date 2021-08-27 22:55
 */
public class LazySingleton {

    private LazySingleton() {
    }

    /**
     * 保证 instance 在所有线程中同步
     */
    private static volatile LazySingleton instance = null;

    /**
     * 获取懒汉式实例
     *
     * @return 懒汉式实例
     */
    public static LazySingleton getInstance() {
        //getInstance 方法前加同步
        if (instance == null) {
            synchronized (LazySingleton.class) {
                instance = new LazySingleton();
            }
        }
        return instance;
    }
}
